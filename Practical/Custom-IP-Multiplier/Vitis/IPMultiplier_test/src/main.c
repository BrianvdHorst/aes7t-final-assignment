#include "xil_types.h"
#include "xparameters.h"

sint32 *baseaddr_p = (sint32 *)XPAR_MY_MULTIPLIER_0_S00_AXI_BASEADDR;

int main(){
	xil_printf("Multiplier Test\n\r");

	// Write mulitplier inputs to register 0
	*(baseaddr_p+0) = 0x00030003;
	xil_printf("Wrote: 0x%08x \n\r", (baseaddr_p+0));
	xil_printf("Read: 0x%08x \n\r", (baseaddr_p+1));
	xil_printf("End of test");

	return 0;

}
