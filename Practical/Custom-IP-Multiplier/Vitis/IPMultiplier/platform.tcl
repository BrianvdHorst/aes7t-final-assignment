# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct C:\git\aes7t-final-assignment\Practical\Custom-IP-Multiplier\Vitis\IPMultiplier\platform.tcl
# 
# OR launch xsct and run below command.
# source C:\git\aes7t-final-assignment\Practical\Custom-IP-Multiplier\Vitis\IPMultiplier\platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {IPMultiplier}\
-hw {C:\git\aes7t-final-assignment\Practical\Custom-IP-Multiplier\Vivado\design_1_wrapper.xsa}\
-proc {ps7_cortexa9_0} -os {standalone} -fsbl-target {psu_cortexa53_0} -out {C:/git/aes7t-final-assignment/Practical/Custom-IP-Multiplier/Vitis}

platform write
platform generate -domains 
platform active {IPMultiplier}
platform generate
platform clean
platform generate
