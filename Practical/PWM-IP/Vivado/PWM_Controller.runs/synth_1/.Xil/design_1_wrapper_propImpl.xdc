set_property SRC_FILE_INFO {cfile:C:/git/aes7t-final-assignment/Practical/PWM-IP/Vivado/PWM_Controller.srcs/constrs_1/new/pwm_io.xdc rfile:../../../PWM_Controller.srcs/constrs_1/new/pwm_io.xdc id:1} [current_design]
set_property src_info {type:XDC file:1 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M14 [get_ports PWM0]
set_property src_info {type:XDC file:1 line:6 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M15 [get_ports PWM1]
set_property src_info {type:XDC file:1 line:10 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN G14 [get_ports PWM2]
set_property src_info {type:XDC file:1 line:14 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN D18 [get_ports PWM3]
