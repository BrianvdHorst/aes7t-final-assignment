# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct C:\git\aes7t-final-assignment\Practical\Exercise-4-Custom-IP\Vitis\Hardware_Platform\platform.tcl
# 
# OR launch xsct and run below command.
# source C:\git\aes7t-final-assignment\Practical\Exercise-4-Custom-IP\Vitis\Hardware_Platform\platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {Hardware_Platform}\
-hw {C:\git\aes7t-final-assignment\Practical\Exercise-4-Custom-IP\Vivado\Hardware_Platform.xsa}\
-fsbl-target {psu_cortexa53_0} -out {C:/git/aes7t-final-assignment/Practical/Exercise-4-Custom-IP/Vitis}

platform write
domain create -name {standalone_ps7_cortexa9_0} -display-name {standalone_ps7_cortexa9_0} -os {standalone} -proc {ps7_cortexa9_0} -runtime {cpp} -arch {32-bit} -support-app {empty_application}
platform generate -domains 
platform active {Hardware_Platform}
domain active {zynq_fsbl}
domain active {standalone_ps7_cortexa9_0}
platform generate -quick
bsp reload
domain active {zynq_fsbl}
bsp reload
platform generate
