set_property SRC_FILE_INFO {cfile:{c:/git/aes7t-final-assignment/Practical/Exercise-4-Custom-IP-2020.1/Vivado/Led Controller.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0/design_1_processing_system7_0_0/design_1_processing_system7_0_0_in_context.xdc} rfile:{../../../Led Controller.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0/design_1_processing_system7_0_0/design_1_processing_system7_0_0_in_context.xdc} id:1 order:EARLY scoped_inst:design_1_i/processing_system7_0} [current_design]
set_property SRC_FILE_INFO {cfile:{C:/git/aes7t-final-assignment/Practical/Exercise-4-Custom-IP-2020.1/Vivado/Led Controller.srcs/constrs_1/new/led_constraints.xdc} rfile:{../../../Led Controller.srcs/constrs_1/new/led_constraints.xdc} id:2} [current_design]
current_instance design_1_i/processing_system7_0
set_property src_info {type:SCOPED_XDC file:1 line:2 export:INPUT save:INPUT read:READ} [current_design]
create_clock -period 10.000 [get_ports {}]
current_instance
set_property src_info {type:XDC file:2 line:1 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M14        [ get_ports {LEDs_out[0]}]
set_property src_info {type:XDC file:2 line:3 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M15        [ get_ports {LEDs_out[1]}]
set_property src_info {type:XDC file:2 line:5 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN G14        [ get_ports {LEDs_out[2]}]
set_property src_info {type:XDC file:2 line:7 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN D18        [ get_ports {LEDs_out[3]}]
