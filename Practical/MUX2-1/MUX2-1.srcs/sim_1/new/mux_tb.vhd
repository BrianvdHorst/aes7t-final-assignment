----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/19/2021 03:03:07 PM
-- Design Name: 
-- Module Name: mux_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_tb is
--  Port ( );
end mux_tb;

architecture Behavioral of mux_tb is

Component mux 
    Port ( a : in STD_LOGIC;
           b : in STD_LOGIC;
           s : in STD_LOGIC;
           o : out STD_LOGIC);
End Component;

signal tb_a, tb_b, tb_s, tb_o : STD_LOGIC;

begin

    mux_tb : mux PORT MAP (tb_a, tb_b, tb_s, tb_o);
    
    process begin
    
        wait for 20ns;
        
        tb_a <= '1' ;
        tb_b <= '0';
        tb_s <= '1';
        -- tb_o == 1
        
        wait for 20ns;
        
        tb_a <= '0' ;
        tb_b <= '1';
        tb_s <= '0';
        -- tb_o == 1
        
        wait;
    
    end process;

end Behavioral;
