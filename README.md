# AES7T Final Assignment

## Basic explanation of the assignment

The assignment must use a register to communicate between the Programming Logic (PL) and Processing System (PS), to do so the decision was made to use one register to communicate two values from the PS -> PL. The first value is which LEDs are currently active on the hardware, and the second value is used to determine the brightness of the earlier mentioned LEDs which will be represented by a PWM value. 

### First 'register'
Will be used as a simple state indicator. The Zybo contains 4 LEDs and the register will communicate in 4 bits which will be turned on (1) and which will be turned off (0)

| LED   | LD3 | LD2 | LD 1| LD0 |
|-------|-----|-----|-----|-----|
| Bit   | 0   | 0   | 0   | 0   |

### Second 'register'
Will be used to communicate the PWM signal strength from 0 - 255. 

| Bit   | 8   | 7  | 6  | 5  | 4 | 3 | 2 | 1 |
|-------|-----|----|----|----|---|---|---|---|
| Value | 128 | 64 | 32 | 16 | 8 | 4 | 2 | 1 |
| 255   | 1   | 1  | 1  | 1  | 1 | 1 | 1 | 1 |
| 127   | 0   | 1  | 1  | 1  | 1 | 1 | 1 | 1 |
| 60    | 0   | 0  | 1  | 1  | 1 | 1 | 0 | 0 |
| 48    | 0   | 0  | 1  | 1  | 0 | 0 | 0 | 0 |
| 0     | 0   | 0  | 0  | 0  | 0 | 0 | 0 | 0 |

<sub>PWM Example values</sub>

## Planning:

- [X] Week 1
    - [X] Create a basic flowchart
    - [X] Create project
- [X] Week 2
    - [X] Recreate assignment 4A *(Is it still necessary to use an IP? **Yes**)*
    - [ ] Split the register
    - [X] Read registers that communicate with the LEDs
    - [ ] Read registers that determine the PWM signal height
    - [X] Setup Git
- [X] Week 3
    - [ ] Start on the PS-side *(Using the following libraries: Xil_IO, Xil_In, Xil_Out & XGPIO)*
- [X] Week 4
    - [X] Testing
    - [X] Debugging 
    - [X] PS and PL communicate as expected
    - [ ] Update the flowchart (add to git)
- [ ] Week 5
    - [X] Testbench Tutorial
    - [ ] Determine which aspects to test
- [ ] Week 6
    - [ ] Create the testbench for the PL
- [ ] Week 7 & 8
    - [ ] Documentation
    - [ ] Make appointment for presentation
- [ ] Week 9
    - [ ] Presentation
- [ ] Graduate :tada:

### Todo for week  (06-12-21, Week 5)
- [X] Prepare questions for Lizeth
- [ ] Report progress, Context
- [X] Follow: https://canvas.fontys.nl/courses/3218/assignments/19485?module_item_id=129402
- [X] If not working on hello world: https://canvas.fontys.nl/courses/3218/modules/items/135225
- [X] Add external IO (Not needed)
- [X] Counter

## Unsolved Questions

### Old Questions

- [X] How do I testbench an IP? Is it inside the IP? *Inside the IP*
- [X] Bugs, Bugs, Bugs and more bugs...
- [X] Which version of Vivado is stable enough?
- [X] What is expected of the report? How deep do I need to go? *https://canvas.fontys.nl/courses/3218/pages/final-project-guidelines?module_item_id=141844*

## Modification of the Makefiles

Replace code in the following make files:
- Hardware_Platform\hw\drivers\led_controller_v1_0\src
- Hardware_Platform\ps7_cortexa9_0\standalone_ps7_cortexa9_0\bsp\ps7_cortexa9_0\libsrc\led_controller_v1_0\src
- Hardware_Platform\zynq_fsbl\zynq_fsbl_bsp\ps7_cortexa9_0\libsrc\led_controller_v1_0\src

With the following: (USE TABS INSTEAD OF SPACES FOR IDENTATIONS)

    COMPILER=
    ARCHIVER=
    CP=cp
    COMPILER_FLAGS=
    EXTRA_COMPILER_FLAGS=
    LIB=libxil.a

    RELEASEDIR=../../../lib
    INCLUDEDIR=../../../include
    INCLUDES=-I./. -I${INCLUDEDIR}

    INCLUDEFILES=*.h
    LIBSOURCES=*.c
    OBJECTS = $(addsuffix .o, $(basename $(wildcard *.c)))
    ASSEMBLY_OBJECTS = $(addsuffix .o, $(basename $(wildcard *.S)))

    libs:
        echo "Compiling myip..."
        $(COMPILER) $(COMPILER_FLAGS) $(EXTRA_COMPILER_FLAGS) $(INCLUDES) $(LIBSOURCES)
        $(ARCHIVER) -r ${RELEASEDIR}/${LIB} ${OBJECTS} ${ASSEMBLY_OBJECTS}
        make clean

    include:
        ${CP} $(INCLUDEFILES) $(INCLUDEDIR)

    clean:
        rm -rf ${OBJECTS}{ASSEMBLY_OBJECTS}

## Zybo constraints (XDC)

    set_property PACKAGE_PIN M14        [ get_ports {LEDs_out[0]}]
    set_property IOSTANDARD LVCMOS33    [ get_ports {LEDs_out[0]}]
    set_property PACKAGE_PIN M15        [ get_ports {LEDs_out[1]}]
    set_property IOSTANDARD LVCMOS33    [ get_ports {LEDs_out[1]}]
    set_property PACKAGE_PIN G14        [ get_ports {LEDs_out[2]}]
    set_property IOSTANDARD LVCMOS33    [ get_ports {LEDs_out[2]}]
    set_property PACKAGE_PIN D18        [ get_ports {LEDs_out[3]}]
    set_property IOSTANDARD LVCMOS33    [ get_ports {LEDs_out[3]}]