#include "xparameters.h"
#include "xil_io.h"
#include "xbasic_types.h"

//#define MY_PWM XPAR_MY_PWM_CORE_0_S00_AXI_BASEADDR    // link to correct name
#define MY_PWM 0x43C00000 //This value is found in the Address editor tab in Vivado (next to Diagram tab)

int main(){
    int num=0;
    int i;

    while(1){
        if(num == 1024)
             num = 0;
        else
             num++;

        xil_printf("My_pwm) \n\r");
        Xil_Out32(MY_PWM, num);
        xil_printf("My_pwm+4) \n\r");
        Xil_Out32((MY_PWM+4), num);
        xil_printf("My_pwm+8) \n\r");
        Xil_Out32((MY_PWM+8), num);
        xil_printf("My_pwm+12) \n\r");
        Xil_Out32((MY_PWM+12), num);

    for(i=0;i<35000; i++);     }
}
