library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity led_controller is
  port(
    clk : in std_logic;
    a   : in std_logic_vector(15 downto 0); -- Determines the LEDs affected by it
    b   : in std_logic_vector(15 downto 0); -- PWM Signal Strength
    p   : out std_logic_vector(3 downto 0);
    c   : out integer
  );
end led_controller;

architecture IMP of led_controller is

constant PWM_COUNTER_MAX : integer := 1024;
signal counter : integer := 0;
  
begin

  process (clk)
  --variable counter : integer := 0;
  -- variable direction : integer := 0;
  variable my_counter : std_logic_vector(31 downto 0);
  variable led_states : std_logic_vector(3 downto 0);
  
  begin
  
    -- counter: 0 - 1023 loop
    if(rising_edge(clk)) then
        if(counter < (PWM_COUNTER_MAX-1)) then
            counter <= counter + 1;
        else
            counter <= 0;
        end if;
    end if;
    
    c <= counter;
    
    my_counter := std_logic_vector(to_unsigned(counter, my_counter'length));
    led_states := a(3 downto 0);
    
    if(b < my_counter) then
       p <= led_states AND "1111";
    else
       p <= "0000";
    end if;
    
  end process;
end IMP;