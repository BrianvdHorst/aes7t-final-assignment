library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity led_controller_tb is
--  Port ( );
end led_controller_tb;

architecture Behavioral of led_controller_tb is

Component led_controller
    Port (
        clk : in std_logic;
        a   : in std_logic_vector(15 downto 0); -- LEDS
        b   : in std_logic_vector(15 downto 0); -- PWM Signal Strength
        p   : out std_logic_vector(3 downto 0);
        c   : out integer
    );
End component;

signal tb_clk : std_logic := '0';
signal tb_a, tb_b : std_logic_vector(15 downto 0);
signal tb_p :  std_logic_vector(3 downto 0);
signal tb_c : integer;

begin

    led_controller_tb : led_controller PORT MAP (tb_clk, tb_a, tb_b, tb_p, tb_c);
   
    -- NEEDED FOR SIMULATION
    -- https://stackoverflow.com/questions/17904514/vhdl-how-should-i-create-a-clock-in-a-testbench
    tb_clk <=  '1' after 10ns when tb_clk = '0' else
        '0' after 10ns when tb_clk = '1';
    
    process begin
 
        -- TEST
        -- RESULT: Expecting all the LEDs On
        tb_a <= "0000000000001111";
        tb_b <= "0000000000000000";
        wait for 20.48us;
        
        -- RESULT: Expecting all the LEDs Off
        tb_a <= "0000000000000000";
        tb_b <= "0000000000000000";
        wait for 20.48us;
        
        -- RESULT: Expecting all the LEDs to be Off except One
        tb_a <= "0000000000001000";
        tb_b <= "0000000000000000";
        wait for 20.48us;
        
        -- RESULT: 0% PWM
        tb_a <= "0000000000001000";
        tb_b <= "0000000010000000";
        wait for 20.48us;
        
        -- RESULT: 50% PWM
        tb_a <= "0000000000001000";
        tb_b <= "0000000001000000";
        wait for 20.48us;
        
                
    end process;
end Behavioral;
