# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct C:\git\aes7t-final-assignment\Assignment\Vitis\Casus_System\platform.tcl
# 
# OR launch xsct and run below command.
# source C:\git\aes7t-final-assignment\Assignment\Vitis\Casus_System\platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {Casus_System}\
-hw {C:\git\aes7t-final-assignment\Assignment\Vivado\design_1_wrapper.xsa}\
-proc {ps7_cortexa9_0} -os {standalone} -fsbl-target {psu_cortexa53_0} -out {C:/git/aes7t-final-assignment/Assignment/Vitis}

platform write
platform generate -domains 
platform active {Casus_System}
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform active {Casus_System}
platform config -updatehw {C:/git/aes7t-final-assignment/Assignment/Vivado/design_1_wrapper.xsa}
platform generate
platform clean
platform generate
platform active {Casus_System}
platform config -updatehw {C:/git/aes7t-final-assignment/Assignment/Vivado/design_1_wrapper.xsa}
platform generate -domains 
platform config -updatehw {C:/git/aes7t-final-assignment/Assignment/Vivado/design_1_wrapper.xsa}
platform clean
platform generate
platform config -updatehw {C:/git/aes7t-final-assignment/Assignment/Vivado/design_1_wrapper.xsa}
platform clean
platform generate
platform active {Casus_System}
platform config -updatehw {C:/git/aes7t-final-assignment/Assignment/Vivado/design_1_wrapper.xsa}
platform generate -domains 
platform config -updatehw {C:/git/aes7t-final-assignment/Assignment/Vivado/design_1_wrapper.xsa}
platform generate -domains 
platform active {Casus_System}
platform config -updatehw {C:/git/aes7t-final-assignment/Assignment/Vivado/design_1_wrapper.xsa}
platform generate -domains standalone_domain,zynq_fsbl 
platform config -updatehw {C:/git/aes7t-final-assignment/Assignment/Vivado/design_1_wrapper.xsa}
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform config -updatehw {C:/git/aes7t-final-assignment/Assignment/Vivado/design_1_wrapper.xsa}
platform clean
platform generate
