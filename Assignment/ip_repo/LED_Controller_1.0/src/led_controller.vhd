library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity led_controller is
  port(
    clk : in std_logic;
    a   : in std_logic_vector(15 downto 0); -- Determines the LEDs affected by it
    b   : in std_logic_vector(15 downto 0); -- PWM Signal Strength
    p   : out std_logic_vector(3 downto 0)
  );
end led_controller;

architecture IMP of led_controller is

constant PWM_COUNTER_MAX : integer := 1024;
  
begin
  process (clk)
  variable counter : integer := 0;
  variable direction : integer := 0;
  variable my_counter :std_logic_vector(31 downto 0);
  
  begin
    
    -- if(rising_edge(clk)) then
    --     if(counter < (PWM_COUNTER_MAX-1)) then
    --         counter := counter + 1;
    --     else
    --         counter := 0;
    --     end if;
    -- end if;
    
    -- my_counter := std_logic_vector(to_unsigned(counter, my_counter'length));
    
    -- if(b < my_counter) then
    --    p <= "0000";
    -- else
    --    p <= "0000";
    -- end if;
    -- PWM Here
    p <= "0100";
  end process;
end IMP;