library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity led_controller is
  port(
    clk : in std_logic;
    a   : in std_logic_vector(15 downto 0);
    b   : in std_logic_vector(15 downto 0);
    p   : out std_logic_vector(3 downto 0)
  );
end led_controller;

architecture IMP of led_controller is
  
begin
  process (clk)
  begin
    p <= "1111";
    -- PWM Here
  end process;
end IMP;