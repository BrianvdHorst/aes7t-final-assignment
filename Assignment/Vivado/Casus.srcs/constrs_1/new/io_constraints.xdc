set_property PACKAGE_PIN M14        [ get_ports {LEDs_out[0]}]
set_property IOSTANDARD LVCMOS33    [ get_ports {LEDs_out[0]}]
set_property PACKAGE_PIN M15        [ get_ports {LEDs_out[1]}]
set_property IOSTANDARD LVCMOS33    [ get_ports {LEDs_out[1]}]
set_property PACKAGE_PIN G14        [ get_ports {LEDs_out[2]}]
set_property IOSTANDARD LVCMOS33    [ get_ports {LEDs_out[2]}]
set_property PACKAGE_PIN D18        [ get_ports {LEDs_out[3]}]
set_property IOSTANDARD LVCMOS33    [ get_ports {LEDs_out[3]}]

set_property -dict { PACKAGE_PIN K17 IOSTANDARD LVCMOS33 } [get_ports {clk_in }]; #IO_L12P_T1_MRCC_35 Sch=sysclk
create_clock -add -name sys_clk_pin -period 8.00 -waveform {0 4} [get_ports { clk_in }];